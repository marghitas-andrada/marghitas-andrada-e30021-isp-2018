
package printnumberinword;

public class PrintNumberInWord {
    public static void main(String[] args) {
        int number = 5; 
        useNestedIf(number); 
        useSwitchCase(number); 
    } 
 private static void useNestedIf(int number) 
     { 
       String numberStr = null;     
  
        switch (number) {
            case 0:
                numberStr = "ZERO";
                break;
            case 1:
                numberStr = "ONE";
                break;
            case 2:
                numberStr = "TWO";
                break;
            case 3:
                numberStr = "THREE";
                break;
            case 4:
                numberStr = "FOUR";
                break;
            case 5:
                numberStr = "FIVE";
                break;
            case 6:
                numberStr = "SEX";
                break;
            case 7: 
                numberStr = "SEVEN";
                break;
            case 8:
                numberStr = "EIGHT";
                break;
            case 9:
                numberStr = "NINE";
                break;
            default:
                numberStr = "OTHER";
                break;
        }
      
 System.out.println("(a) Use a \"nested-if\" statement: " + numberStr);
    }
       private static void useSwitchCase(int number) 
    { 
        String numberStr = null; 
         switch (number) { 
            case 0:  numberStr = "ZERO";  break; 
           case 1:  numberStr = "ONE";   break; 
           case 2:  numberStr = "TWO";   break; 
            case 3:  numberStr = "THREE"; break; 
             case 4:  numberStr = "FOUR";  break; 
            case 5:  numberStr = "FIVE";  break; 
            case 6:  numberStr = "SEX";   break; 
case 7:  numberStr = "SEVEN"; break; 
             case 8:  numberStr = "EIGHT"; break; 
case 9:  numberStr = "NINE";  break; 
            default: numberStr = "OTHER"; break; 
        } 
        System.out.println("(b) Use a \"switch-case\" statement: " + numberStr); 
    } 
     
 } 

